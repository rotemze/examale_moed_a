<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 22],
        ];
    }
	public static function getCategoryesid()
	{
		$allCategoryes = self::find()->all();
		$allCategoryesArray = ArrayHelper::
					map($allCategoryes, 'id', 'name');
		return $allCategoryesArray;						
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
