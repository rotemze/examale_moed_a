<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$approved = $auth->createRole('approved');
		$auth->add($approved);
		
		$categoryManager = $auth->createRole('categoryManager');
		$auth->add($categoryManager);
		
		$guest = $auth->createRole('guest');
		$auth->add($guest);
	}

	public function actionTmpermissions()
	{
			
	}


	public function actionTlpermissions()
	{
				
	}
	
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;


		$updateStatus = $auth->createPermission('updateStatus');
		$updateStatus->description = 'Admin can update status';
		$auth->add($updateStatus);
		
		
	}
	
	public function actionApprovedpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createActivity = $auth->createPermission('createActivity');
		$createActivity->description = 'Team leader can create new activity';
		$auth->add($createActivity);
		
	}
	
	public function actionCmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateActivity = $auth->createPermission('updateActivity');
		$updateActivity->description = 'categoryManager can update status';
		$auth->add($updateActivity);
		
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$categoryManager = $auth->getRole('categoryManager');‏		
		$updateActivity = $auth->getPermission('updateActivity');
		$auth->addChild($categoryManager, $updateActivity);
		
		
		$approved = $auth->getRole('approved');‏
		$createActivity = $auth->getPermission('createActivity');
		$auth->addChild($approved, $createActivity);
		
		
		
	}
}