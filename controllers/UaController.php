<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class UaController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateActivity = $auth->getPermission('updateActivity');
		//$auth->remove($updateActivity);
		
		$rule = new \app\rbac\OwnActivityRule;
		$auth->add($rule);
				
		$updateActivity->rule_name = $rule->name;		
		$auth->add($updateActivity);	
	}
}