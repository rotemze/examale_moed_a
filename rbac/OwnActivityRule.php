<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnActivityRule extends Rule
{
	public $name = 'OwnActivityRule';

	public function execute($user, $item, $params)
	{
		$userModel = User::findIdentity($user);
		
		if (!Yii::$app->user->isGuest) {
			//die('logged in user ='.$user.'worked on user = '.$params['user']->id);
			//die (isset($params['user']) ? $params['user']->id == $user : false); 
			return isset($params['activity']) ? $params['activity']->categoryid == $userModel->categoryid : false;
		}
		return false;
	}
}