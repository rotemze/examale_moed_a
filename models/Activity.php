<?php

namespace app\models;
use app\models\Status;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryid
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }
	
	public static function getActivities(){
		$Activities = ArrayHelper::
				map(Activity::find()->all(), 'id', 'title');
		
		return $Activities;

	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categoryid'], 'required'],
            [['categoryid', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 22],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryid' => 'Categoryid',
            'statusId' => 'Status ID',
        ];
    }
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

       if ($this->isNewRecord)
            $this->statusId = 2;
        return $return;
    }
}
